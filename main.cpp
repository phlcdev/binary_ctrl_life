#include <QtGui/QApplication>
#include <QtGui/QWSServer>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

#ifdef Q_WS_QWS
    a.setOverrideCursor( QCursor( Qt::BlankCursor ) );
    QWSServer::setCursorVisible( false );
    QWSServer::setBackground(QBrush(Qt::black));
#endif

    MainWindow w;
    w.showFullScreen();
    return a.exec();
}
