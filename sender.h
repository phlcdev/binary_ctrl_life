#ifndef SENDER_H
#define SENDER_H

#include <QObject>
#include <QtNetwork/QtNetwork>

class Sender : public QObject
{
    Q_OBJECT
public:
    Sender(QObject *parent = 0);

signals:

public slots:
    void EnviarDados(QByteArray ba);

private:
    QUdpSocket *udpSocket;
};

#endif // SENDER_H
