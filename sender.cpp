#include "sender.h"

Sender::Sender(QObject *parent) :
    QObject(parent)
{
    udpSocket = new QUdpSocket(this);
}

void Sender::EnviarDados(QByteArray ba)
{
    QHostAddress hostip("192.168.0.22");
    udpSocket->writeDatagram(ba.data(), ba.size(),
                             hostip, 110);
}
