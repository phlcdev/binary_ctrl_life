#-------------------------------------------------
#
# Project created by QtCreator 2012-12-27T00:39:39
#
#-------------------------------------------------

QT       += core
QT       += gui
QT       += network

TARGET = Binary_Ctrl_Life

TEMPLATE = app

INCLUDEPATH += /usr/local/qt/include/Qt/

SOURCES += main.cpp\
        mainwindow.cpp \
    sender.cpp

HEADERS  += mainwindow.h \
    sender.h

FORMS    += mainwindow.ui
