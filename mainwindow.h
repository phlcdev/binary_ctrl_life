#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMouseEvent>
#include <QPoint>
#include <QCursor>
#include <QtGui>
#include <QtCore>
#include <qevent.h>
#include "sender.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void paintEvent(QPaintEvent *e);
    void mouseMoveEvent(QMouseEvent *event);

private:
    Ui::MainWindow *ui;
    int mx, my;
    Sender *sender;
};

#endif // MAINWINDOW_H
