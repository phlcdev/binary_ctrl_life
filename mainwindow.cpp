#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    sender = new Sender(this);
    ui->setupUi(this);
    setMouseTracking(true);
    this->installEventFilter(this);
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
    mx = mouseEvent->pos().x();
    my = mouseEvent->pos().y();
    QByteArray datagram = QByteArray::number(mx) + "," + QByteArray::number(my);
    sender->EnviarDados(datagram);
    this->update();
}

void MainWindow::paintEvent(QPaintEvent *e)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setPen(QPen(Qt::black, 2));
    painter.drawLine(mx,0,mx,240); // vertical dx
    painter.drawLine(0,my,320,my); // horizontal dy
}


MainWindow::~MainWindow()
{
    delete ui;
}
